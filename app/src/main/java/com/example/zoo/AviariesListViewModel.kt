package com.example.zoo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.zoo.data.AviariesList
import com.example.zoo.data.Aviary
import com.example.zoo.repository.AviaryRepository

class AviariesListViewModel : ViewModel() {

    var aviariesList : MutableLiveData<AviariesList> = MutableLiveData()

    private var aviaryCurrent : Aviary = Aviary()
    val aviary : Aviary
        get() = aviaryCurrent

    private var observer : Observer<AviariesList> = Observer<AviariesList>
    {
            newList ->
        newList?.let {
            Log.d(MyConstants.TAG,"Получен список AviariesListViewModel от AviaryRepository")
            aviariesList.postValue(newList)
        }
    }

    init {
        AviaryRepository.getInstance().aviary.observeForever {
            aviaryCurrent = it
            Log.d(MyConstants.TAG, "Получили Aviary в AviaryInfoViewModel")
        }
        AviaryRepository.getInstance().aviariesList.observeForever(observer)
        Log.d(MyConstants.TAG, "Подписались AviariesListViewModel к AviaryRepository")
    }

    fun setAviary(aviary: Aviary) {
        AviaryRepository.getInstance().setCurrentAviary(aviary)
    }

}