package com.example.zoo

import android.app.Application
import android.content.Context
import com.example.zoo.repository.AnimalRepository
import com.example.zoo.repository.AviaryRepository

class AppAviaryIntendApplication : Application() {

    /*override fun onCreate() {
        super.onCreate()
        AviaryRepository.getInstance()
        //val context: Context = AnteySupplierIntendApplication.applicationContext()
    }*/

    init {
        instance = this
        //AviaryRepository.getInstance()
    }

    companion object {
        private var instance: AppAviaryIntendApplication? = null
        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }
}