package com.example.zoo

object MyConstants {
    const val EXTRA_AVIARY_LIST_ID = "com.example.zoo.aviary_list_id"
    const val ANIMAL_LIST_FRAGMENT_TAG = "com.example.zoo.animal_list"
    const val ANIMAL_INFO_FRAGMENT_TAG = "com.example.zoo.animal_info"
    const val AVIARY_LIST_FRAGMENT_TAG = "com.example.zoo.aviary_list"
    const val AVIARY_INFO_FRAGMENT_TAG = "com.example.zoo.aviary_info"
    const val TAG = "com.example.zoo.log_tag"

}