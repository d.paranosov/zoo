package com.example.zoo

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.zoo.MyConstants.AVIARY_INFO_FRAGMENT_TAG
import com.example.zoo.MyConstants.AVIARY_LIST_FRAGMENT_TAG
import com.example.zoo.MyConstants.EXTRA_AVIARY_LIST_ID
import com.example.zoo.data.Animal
import com.example.zoo.data.Aviary
import com.example.zoo.repository.AnimalRepository
import com.example.zoo.repository.AviaryRepository


class MainActivity : AppCompatActivity() {

    private var miAdd : MenuItem? = null
    private var miChoose : MenuItem? = null
    private var miDelete : MenuItem? = null
    private var miChange : MenuItem? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showAviariesList()
        //showAnimalsList()

        val callback = object : OnBackPressedCallback(true)
        {
            override fun handleOnBackPressed() {
                checkLogout()
            }
        }

        onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        saveData()
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onRestoreInstanceState(
        savedInstanceState: Bundle?,
        persistentState: PersistableBundle?
    ) {
        loadData()
        super.onRestoreInstanceState(savedInstanceState, persistentState)
    }

    private fun loadData() {
        AviaryRepository.getInstance().loadAviaries()
    }

    private fun saveData() {
        AviaryRepository.getInstance().saveAviaries()
    }

    override fun onStop() {
        saveData()
        super.onStop()
    }

    private fun checkLogout() {
        AlertDialog.Builder(this)
            .setTitle("Выход!")
            .setMessage("Вы действительно хотите выйти из приложения?")

            .setPositiveButton("ДА") { _, _ ->
                finish()
            }
            .setNegativeButton("НЕТ", null)
            .setCancelable(true)
            .create()
            .show()
    }

    fun checkAviaryDelete(aviary: Aviary? = AviaryRepository.getInstance().aviary.value) {
        if (aviary == null) return
        val s = aviary.name
        AlertDialog.Builder(this)
            .setTitle("УДАЛЕНИЕ!")
            .setMessage("Вы действительно хотите удалить вольер $s ?")
            .setPositiveButton("ДА") { _, _ ->
                AviaryRepository.getInstance().deleteAviary(aviary)
            }
            .setNegativeButton("НЕТ", null)
            .setCancelable(true)
            .create()
            .show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        /*val toast = Toast.makeText(applicationContext, AppAviaryIntendApplication.applicationContext().toString(), Toast.LENGTH_LONG)
        toast.show()*/
        inflater.inflate(R.menu.main_menu, menu)

        miAdd=menu.findItem(R.id.miAdd)
        miChoose=menu.findItem(R.id.miChoose)
        miDelete=menu.findItem(R.id.miDelete)
        miChange=menu.findItem(R.id.miChange)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.miAdd -> {
                showNewAviary()
                true
            }
            R.id.miChoose -> {
                showAnimals()
                true
            }
            R.id.miDelete -> {
                checkAviaryDelete()
                true
            }
            R.id.miChange -> {
                showAviaryInfo()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun showAnimals() {
        val intent = Intent(this, AnimalActivity::class.java)
        //intent.putExtra(EXTRA_AVIARY_LIST_ID, AviaryRepository.getInstance().aviary.value?.id)
        startActivity(intent)
    }

    fun showNewAviary() {
        AviaryRepository.getInstance().newAviary()
        showAviaryInfo()
    }

    fun showAviariesList() {
        //Log.d(MyConstants.TAG, "${AviaryRepository.getInstance().aviary.value?.id ?: "Nothing!!!"}")
        //Log.d(MyConstants.TAG, "${AviaryRepository.getInstance().aviary.value ?: "Nothing Too !!!"}")
        miAdd?.isVisible=true
        miChoose?.isVisible=true
        miDelete?.isVisible=true
        miChange?.isVisible=true
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame, AviariesListFragment.getInstance(), AVIARY_LIST_FRAGMENT_TAG)
            .commit()
    }

    fun showAviaryInfo() {
        miAdd?.isVisible=false
        miChoose?.isVisible=false
        miDelete?.isVisible=false
        miChange?.isVisible=false
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame, AviaryInfoFragment.getInstance(), AVIARY_INFO_FRAGMENT_TAG)
            //.addToBackStack(null)
            .commit()
    }

}