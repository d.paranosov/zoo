package com.example.zoo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.zoo.data.AnimalsList
import com.example.zoo.data.Aviary
import com.example.zoo.repository.AviaryRepository

class AviaryInfoViewModel : ViewModel() {

    var aviary : MutableLiveData<Aviary> = MutableLiveData()
    init {
        AviaryRepository.getInstance().aviary.observeForever {
            aviary.postValue(it)
        }
    }

    fun save(name: String = "",
            animals: AnimalsList = AnimalsList()
    ) {
        if (aviary.value == null) aviary.value = Aviary()
        aviary.value!!.name = name
        aviary.value!!.animals = animals
        AviaryRepository.getInstance().updateAviary(aviary.value!!)
    }

}