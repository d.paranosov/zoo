package com.example.zoo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.zoo.MyConstants.TAG
import com.example.zoo.data.Animal
import com.example.zoo.data.AnimalsList
import com.example.zoo.repository.AnimalRepository
import com.example.zoo.repository.AviaryRepository

class AnimalsListViewModel : ViewModel() {

    var animalsList : MutableLiveData<AnimalsList> = MutableLiveData()

    private var animalCurrent : Animal = Animal()
    val animal : Animal
        get() = animalCurrent

    private var observer : Observer<AnimalsList> = Observer<AnimalsList>
    {
            newList ->
        newList?.let {
            Log.d(TAG,"Получен список AnimalsListViewModel от AnimalRepository")
            animalsList.postValue(newList)
        }
    }

    init {
        AnimalRepository.getInstance().animal.observeForever {
            animalCurrent = it
            Log.d(TAG, "Получили Animal в AnimalInfoViewModel")
        }
        AnimalRepository.getInstance().animalsList.observeForever(observer)
        Log.d(TAG, "Подписались AnimalsListViewModel к AnimalRepository")
    }

    fun setAnimal(animal: Animal) {
        AnimalRepository.getInstance().setCurrentAnimal(animal)
    }

}