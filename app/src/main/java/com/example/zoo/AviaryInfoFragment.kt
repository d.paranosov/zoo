package com.example.zoo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.zoo.data.Aviary
import com.example.zoo.repository.AviaryRepository
import java.util.*

class AviaryInfoFragment : Fragment() {

    private lateinit var aviaryInfoViewModel : AviaryInfoViewModel
    private lateinit var etName : EditText
    private lateinit var btnSave : Button
    private lateinit var btnCancel : Button

    companion object {
        private var INSTANCE : AviaryInfoFragment? = null
        fun getInstance(): AviaryInfoFragment {
            if (INSTANCE == null){
                INSTANCE = AviaryInfoFragment()
            }
            return INSTANCE?: throw IllegalStateException("Отображение не создано!")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.aviary_info, container, false)
        etName=view.findViewById(R.id.etAvName)
        btnSave=view.findViewById(R.id.btOk2)
        btnSave.setOnClickListener {
            saveAviary()
            closeFragment()
        }
        btnCancel=view.findViewById(R.id.btnCancel2)
        btnCancel.setOnClickListener {
            closeFragment()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        aviaryInfoViewModel = ViewModelProvider(this).get(AviaryInfoViewModel::class.java)
        aviaryInfoViewModel.aviary.observe(viewLifecycleOwner) {
            updateUI(it)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true)
            {
                override fun handleOnBackPressed() {
                    closeFragment()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    fun saveAviary() {
        //if (AviaryRepository.getInstance().checkAviaryName(etName.text.toString())) {
            aviaryInfoViewModel.save(
                etName.text.toString()
            )
        //}
    }

    fun updateUI(aviary: Aviary) {
        etName.setText(aviary.name)
    }

    private fun closeFragment() {
        (requireActivity() as MainActivity).showAviariesList()
    }
}