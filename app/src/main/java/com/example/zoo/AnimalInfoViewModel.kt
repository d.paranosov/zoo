package com.example.zoo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.zoo.data.Animal
import com.example.zoo.data.Aviary
import com.example.zoo.repository.AnimalRepository
import java.util.*

class AnimalInfoViewModel : ViewModel() {

    var animal : MutableLiveData<Animal> = MutableLiveData()
    init {
        AnimalRepository.getInstance().animal.observeForever {
            animal.postValue(it)
        }
    }

    fun save(name: String = "",
             birthDate : Date = Date(),
             animalClass : String = "") {
        if (animal.value == null) animal.value = Animal()
        animal.value!!.name = name
        animal.value!!.birthDate = birthDate
        animal.value!!.animalClass = animalClass
        //animal.value!!.aviary = aviary
        AnimalRepository.getInstance().updateAnimal(animal.value!!)
    }

}