package com.example.zoo.repository

import android.content.SharedPreferences
import android.util.Log
import androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportCallback
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import com.example.zoo.AnimalActivity
import com.example.zoo.AppAviaryIntendApplication
import com.example.zoo.MainActivity
import com.example.zoo.MyConstants
import com.example.zoo.data.Animal
import com.example.zoo.data.AnimalsList
import com.google.gson.Gson
import java.util.*

class AnimalRepository {

    companion object {
        private var INSTANCE : AnimalRepository?=null

        fun getInstance(): AnimalRepository {
            if (INSTANCE == null) {
                INSTANCE = AnimalRepository()
            }
            return INSTANCE?:
            throw IllegalStateException("Репозиторий AnimalRepository не инициализирован")
        }
    }

    var animalsList: MutableLiveData<AnimalsList> = MutableLiveData()
    var animal: MutableLiveData<Animal> = MutableLiveData()

    init {
        loadAnimals()
    }

    fun loadAnimals() {
        val jsonString =
            PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext())
                .getString("animalsFrom_${AviaryRepository.getInstance().aviary.value!!.id}", null)
        if (!jsonString.isNullOrBlank()) {
            val st = Gson().fromJson(jsonString, AnimalsList::class.java)
            if (st != null) {
                this.animalsList.postValue(st)
                //AviaryRepository.getInstance().aviary.value!!.animals = st
            }
        }
    }

    fun saveAnimals() {
        val gson = Gson()
        val jsonAnimals = gson.toJson(animalsList.value)
        val preference =
            PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext())
        preference.edit().apply {
            putString("animalsFrom_${AviaryRepository.getInstance().aviary.value!!.id}", jsonAnimals)
            apply()
        }
    }

    fun lengthOfAnimalsList(): Int {
        val jsonString =
            PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext())
                .getString("animalsFrom_${AviaryRepository.getInstance().aviary.value!!.id}", null)
        if (!jsonString.isNullOrBlank()) {
            val st = Gson().fromJson(jsonString, AnimalsList::class.java)
            if (st != null)
                return st.items.size
        }
        return -1
    }

    fun setCurrentAnimal(position: Int) {
        if (animalsList.value == null || animalsList.value!!.items == null ||
            position < 0 || (animalsList.value?.items?.size!! <= position))
            return
        animal.postValue(animalsList.value?.items!![position])
    }

    fun setCurrentAnimal(animal: Animal) {
        this.animal.postValue(animal)
    }

    fun addAnimal(animal: Animal) {
        var animalsListTmp = animalsList
        if (animalsListTmp.value == null)
            animalsListTmp.value = AnimalsList()
        animalsListTmp.value!!.items.add(animal)
        animalsList.postValue(animalsListTmp.value)
        //AviaryRepository.getInstance().aviary.value!!.animals = animalsList.value!!
    }

    fun getPosition(animal: Animal) : Int = animalsList.value?.items?.indexOfFirst {
        it.id == animal.id } ?: -1

    fun updateAnimal(animal: Animal) {
        var animalsListTmp = animalsList
        val position = getPosition(animal)
        if (animalsListTmp.value == null || position < 0)
            addAnimal(animal)
        else {
            animalsListTmp.value!!.items[position] = animal
            animalsList.postValue(animalsListTmp.value)
        }
        //AviaryRepository.getInstance().aviary.value!!.animals = animalsList.value!!
        //Log.d(MyConstants.TAG, "ДЛИННА СПИСКА: ${AviaryRepository.getInstance().aviary.value!!.animals.items.size}")
    }

    fun deleteAnimal(animal: Animal) {
        var animalsListTmp = animalsList
        if (animalsListTmp.value!!.items.remove(animal)) {
            animalsList.postValue(animalsListTmp.value)
        }
        //AviaryRepository.getInstance().aviary.value!!.animals = animalsList.value!!
        //checkOnDelete()
    }

    fun checkOnDelete(): Boolean {
        Log.d(MyConstants.TAG, "Размер списка животных вольера: ${AviaryRepository.getInstance().aviary.value!!.animals.items.size}")
        return AviaryRepository.getInstance().aviary.value!!.animals.items.size == 0
    }

    fun deleteAnimalCurrent() {
        if (animal.value!=null)
            deleteAnimal(animal.value!!)
    }

    fun newAnimal() {
        animal.postValue(Animal())
    }

    fun destroy() {
        INSTANCE = null
    }

}