package com.example.zoo.repository

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import com.example.zoo.AppAviaryIntendApplication
import com.example.zoo.MainActivity
import com.example.zoo.data.Animal
import com.example.zoo.data.AnimalsList
import com.example.zoo.data.AviariesList
import com.example.zoo.data.Aviary
import com.google.gson.Gson

class AviaryRepository {

    companion object {
        private var INSTANCE : AviaryRepository?=null

        fun getInstance(): AviaryRepository {
            if (INSTANCE == null) {
                INSTANCE = AviaryRepository()
            }
            return INSTANCE?:
            throw IllegalStateException("Репозиторий AviaryRepository не инициализирован")
        }
    }

    var aviariesList: MutableLiveData<AviariesList> = MutableLiveData()
    var aviary: MutableLiveData<Aviary> = MutableLiveData()

    init {
        //deleteInFile("642acb69-3f8b-499d-993f-9b43180c8d00")
        loadAviaries()
    }

    fun loadAviaries() {
        val jsonString =
            PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext())
                .getString("aviaries", null)
        if (!jsonString.isNullOrBlank())
            this.aviariesList.postValue(Gson().fromJson(jsonString, AviariesList::class.java))
    }

    fun saveAviaries() {
        val gson = Gson()
        val jsonAviaries = gson.toJson(aviariesList.value)
        val preference =
            PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext())
        preference.edit().apply {
            putString("aviaries", jsonAviaries)
            apply()
        }
    }

/*
    fun loadAnimals() {
        aviariesList.value!!.items.forEach {
            val jsonString =
                PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext())
                    .getString(
                        "animalsFrom_${aviary.value!!.id}",
                        null
                    )
            if (!jsonString.isNullOrBlank()) {
                val st = Gson().fromJson(jsonString, AnimalsList::class.java)
                if (st != null)
                    it.animals = st
            }
        }
    }

    fun saveAnimals() {
        aviariesList.value!!.items.forEach {
            val gson = Gson()
            val jsonAnimals = gson.toJson(it.animals)
            val preference =
                PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext())
            preference.edit().apply {
                putString(
                    "animalsFrom_${aviary.value!!.id}",
                    jsonAnimals
                )
                apply()
            }
        }
    }
 */

    fun deleteInFile(st: String) {
        PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext()).edit()
            .remove("animalsFrom_${st}")
            .apply()
    }

    fun deleteAnimals(aviary: Aviary) {
        PreferenceManager.getDefaultSharedPreferences(AppAviaryIntendApplication.applicationContext()).edit()
            .remove("animalsFrom_${aviary.id}")
            .apply()
    }

    fun setCurrentAviary(position: Int) {
        if (aviariesList.value == null || aviariesList.value!!.items == null ||
            position < 0 || (aviariesList.value?.items?.size!! <= position))
            return
        aviary.postValue(aviariesList.value?.items!![position])
    }

    fun setCurrentAviary(aviary: Aviary) {
        this.aviary.postValue(aviary)
    }

    fun addAviary(aviary: Aviary) {
        var aviariesListTmp = aviariesList
        if (aviariesListTmp.value == null)
            aviariesListTmp.value = AviariesList()
        aviariesListTmp.value!!.items.add(aviary)
        aviariesList.postValue(aviariesListTmp.value)
    }

    fun getPosition(aviary: Aviary) : Int = aviariesList.value?.items?.indexOfFirst {
        it.id == aviary.id } ?: -1

    fun updateAviary(aviary: Aviary) {
        var aviariesListTmp = aviariesList
        val position = getPosition(aviary)
        if (aviariesListTmp.value == null || position < 0)
            addAviary(aviary)
        else {
            aviariesListTmp.value!!.items[position] = aviary
            aviariesList.postValue(aviariesListTmp.value)
        }
    }
    fun deleteAviary(aviary: Aviary) {
        var aviariesListTmp = aviariesList
        if (aviariesListTmp.value!!.items.remove(aviary)) {
            aviariesList.postValue(aviariesListTmp.value)
        }
        deleteAnimals(aviary)
    }

    fun checkAviaryName(name: String): Boolean {
        aviariesList.value!!.items.forEach {
            if (it.name.lowercase() == name.lowercase()) {
                val toast = Toast.makeText(AppAviaryIntendApplication.applicationContext().applicationContext,
                    "Такой вольер уже существует", Toast.LENGTH_SHORT)
                toast.show()
                return false
            }
        }
        return true
    }

    fun deleteAviaryCurrent() {
        if (aviary.value!=null)
            deleteAviary(aviary.value!!)
    }

    fun newAviary() {
        aviary.postValue(Aviary())
    }

}