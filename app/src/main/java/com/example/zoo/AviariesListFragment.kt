package com.example.zoo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zoo.data.Animal
import com.example.zoo.data.AnimalsList
import com.example.zoo.data.AviariesList
import com.example.zoo.data.Aviary

class AviariesListFragment : Fragment() {

    private lateinit var aviariesListViewModel: AviariesListViewModel
    private lateinit var aviariesListRecyclerView: RecyclerView

    companion object {
        private var INSTANCE : AviariesListFragment? = null

        fun getInstance(): AviariesListFragment {
            if (INSTANCE == null) {
                INSTANCE = AviariesListFragment()
            }
            return INSTANCE?: throw IllegalStateException("Отображение списка не создано!")
        }
    }

    private lateinit var viewModel: AviariesListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layoutView = inflater.inflate(R.layout.list_of_aviaries, container, false)
        aviariesListRecyclerView = layoutView.findViewById(R.id.rvAvList)
        aviariesListRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        return layoutView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        aviariesListViewModel = ViewModelProvider(this).get(AviariesListViewModel::class.java)
        aviariesListViewModel.aviariesList.observe(viewLifecycleOwner){
            updateUI(it)
        }
    }
    private inner class AviaryListAdapter(private val orderItems: List<Aviary>) : RecyclerView.Adapter<AviaryHolder>()
    {
        override fun onCreateViewHolder(
            parrent : ViewGroup,
            viewType :Int
        ):AviaryHolder{
            val view = layoutInflater.inflate(R.layout.aviaries_list_element, parrent, false)
            return AviaryHolder(view)
        }

        override fun getItemCount(): Int = orderItems.size

        override fun onBindViewHolder(holder: AviaryHolder, position: Int) {
            holder.bind(orderItems[position])
        }

    }
    private inner class AviaryHolder(view : View)
        : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        private lateinit var aviary: Aviary
        private val nameTextView : TextView = itemView.findViewById(R.id.tvAvName)
        private val clLayout: ConstraintLayout = itemView.findViewById(R.id.clAv)

        fun bind(aviary: Aviary){
            Log.d(MyConstants.TAG, "bind 1 $aviary")
            this.aviary = aviary
            clLayout.setBackgroundColor(context!!.getColor(R.color.white))
            if (aviary.id == aviariesListViewModel.aviary.id)
                clLayout.setBackgroundColor(context!!.getColor(R.color.element))
            nameTextView.text = aviary.name
            Log.d(MyConstants.TAG, "bind 2 $aviary")
        }

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            Log.d(MyConstants.TAG, "AviaryHolder onClick")
            aviariesListViewModel.setAviary(aviary)
            //(requireActivity() as MainActivity).showAviaryInfo()
            (requireActivity() as MainActivity).showAnimals()
            aviariesListRecyclerView.adapter = AviaryListAdapter(aviariesListViewModel.aviariesList.value!!.items)
        }

        override fun onLongClick(v: View?): Boolean {
            aviariesListViewModel.setAviary(aviary)
            //(requireActivity() as MainActivity).checkAviaryDelete(aviary)
            aviariesListRecyclerView.adapter = AviaryListAdapter(aviariesListViewModel.aviariesList.value!!.items)
            return true
        }

    }
    private fun updateUI(aviariesList: AviariesList?=null){
        if (aviariesList==null) return
        aviariesListRecyclerView.adapter = AviaryListAdapter(aviariesList.items)
        aviariesListRecyclerView.layoutManager?.scrollToPosition(aviariesList.items.size)
    }
}