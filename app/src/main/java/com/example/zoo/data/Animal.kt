package com.example.zoo.data

import com.example.zoo.repository.AviaryRepository
import java.util.*

data class Animal(
    val id : UUID = UUID.randomUUID(),
    var name : String = "",
    var birthDate: Date = Date(),
    var animalClass: String = "",
    //var aviary : String = AviaryRepository.getInstance().aviary.value?.name ?: "",
) {
    val age : Int
    get() {
        val gregorianCalendar1 = GregorianCalendar()
        gregorianCalendar1.timeInMillis = birthDate.time
        val gregorianCalendar2 = GregorianCalendar()
        var y = gregorianCalendar2.get(GregorianCalendar.YEAR)-gregorianCalendar1.get(GregorianCalendar.YEAR)
        if (gregorianCalendar2.get(GregorianCalendar.MONTH)<gregorianCalendar1.get(GregorianCalendar.MONTH))
            y--
        return y
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Animal

        if (id != other.id) return false
        if (name != other.name) return false
        if (birthDate != other.birthDate) return false
        if (animalClass != other.animalClass) return false
        //if (aviary != other.aviary) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + birthDate.hashCode()
        result = 31 * result + animalClass.hashCode()
        //result = 31 * result + aviary.hashCode()
        return result
    }
}