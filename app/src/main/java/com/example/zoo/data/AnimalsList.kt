package com.example.zoo.data

import java.util.*

data class AnimalsList(
    val items : MutableList<Animal> = mutableListOf()
)