package com.example.zoo.data

import androidx.lifecycle.MutableLiveData
import java.util.*

data class Aviary(
    val id : UUID = UUID.randomUUID(),
    var name : String = "",
    var animals : AnimalsList = AnimalsList(),
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Aviary

        if (id != other.id) return false
        if (name != other.name) return false
        if (animals != other.animals) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + animals.hashCode()
        return result
    }
}