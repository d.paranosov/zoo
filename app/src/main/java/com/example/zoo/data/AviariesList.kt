package com.example.zoo.data

data class AviariesList(
    val items : MutableList<Aviary> = mutableListOf()
)