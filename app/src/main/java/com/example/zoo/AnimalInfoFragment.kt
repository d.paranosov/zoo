package com.example.zoo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.zoo.data.Animal
import java.util.*

class AnimalInfoFragment : Fragment() {

    private lateinit var animalInfoViewModel : AnimalInfoViewModel
    private lateinit var etName : EditText
    //private lateinit var etAviary : EditText
    private lateinit var etClass : EditText
    private lateinit var dpDate : DatePicker
    private lateinit var btnSave : Button
    private lateinit var btnCancel : Button

    companion object {
        private var INSTANCE : AnimalInfoFragment? = null
        fun getInstance(): AnimalInfoFragment {
            if (INSTANCE == null){
                INSTANCE = AnimalInfoFragment()
            }
            return INSTANCE?: throw IllegalStateException("Отображение не создано!")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.animal_info, container, false)
        etName=view.findViewById(R.id.etName)
        //etAviary=view.findViewById(R.id.aviary)
        etClass=view.findViewById(R.id.anClass)
        dpDate=view.findViewById(R.id.datePicker)
        btnSave=view.findViewById(R.id.btOk)
        btnSave.setOnClickListener {
            saveAnimal()
            closeFragment()
        }
        btnCancel=view.findViewById(R.id.btnCancel)
        btnCancel.setOnClickListener {
            closeFragment()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        animalInfoViewModel = ViewModelProvider(this).get(AnimalInfoViewModel::class.java)
        animalInfoViewModel.animal.observe(viewLifecycleOwner) {
            updateUI(it)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true)
            {
                override fun handleOnBackPressed() {
                    closeFragment()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    fun saveAnimal() {
        val dateBirth = GregorianCalendar(dpDate.year, dpDate.month, dpDate.dayOfMonth)
        animalInfoViewModel.save(
            etName.text.toString(),
            dateBirth.time,
            etClass.text.toString()
            //etAviary.text.toString()
        )
    }

    fun updateUI(animal: Animal) {
        etName.setText(animal.name)
        val dateBirth = GregorianCalendar()
        dateBirth.time=animal.birthDate
        dpDate.updateDate(dateBirth.get(Calendar.YEAR), dateBirth.get(Calendar.MONTH), dateBirth.get(
            Calendar.DAY_OF_MONTH))
        etClass.setText(animal.animalClass)
        //etAviary.setText(animal.aviary)
    }

    private fun closeFragment() {
        (requireActivity() as AnimalActivity).showAnimalsList()
    }

}