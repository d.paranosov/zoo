package com.example.zoo

import android.app.Application
import android.content.Context
import com.example.zoo.repository.AnimalRepository

class AppAnimalIntendApplication : Application() {

    /*override fun onCreate() {
        super.onCreate()
        AnimalRepository.getInstance()
        //val context: Context = AnteySupplierIntendApplication.applicationContext()
    }*/

    init {
        instance = this
        //AnimalRepository.getInstance()
    }

    companion object {
        private var instance: AppAnimalIntendApplication? = null
        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }
}