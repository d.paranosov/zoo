package com.example.zoo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import com.example.zoo.data.Animal
import com.example.zoo.repository.AnimalRepository
import com.example.zoo.repository.AviaryRepository

class AnimalActivity : AppCompatActivity() {

    private var miAnAdd : MenuItem? = null
    private var miAnDelete : MenuItem? = null
    private var miAnChange : MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animal)
        showAnimalsList()

        /*val callback = object : OnBackPressedCallback(true)
        {
            override fun handleOnBackPressed() {
                checkLogout()
            }
        }

        onBackPressedDispatcher.addCallback(this, callback)*/
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        saveData()
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onRestoreInstanceState(
        savedInstanceState: Bundle?,
        persistentState: PersistableBundle?
    ) {
        loadData()
        super.onRestoreInstanceState(savedInstanceState, persistentState)
    }

    private fun loadData() {
        AnimalRepository.getInstance().loadAnimals()
    }

    private fun saveData() {
        AnimalRepository.getInstance().saveAnimals()
    }

    override fun onStop() {
        saveData()
        AnimalRepository.getInstance().destroy()
        super.onStop()
    }

    /*private fun checkLogout() {
        AlertDialog.Builder(this)
            .setTitle("Выход!")
            .setMessage("Вы действительно хотите выйти из приложения?")

            .setPositiveButton("ДА") { _, _ ->
                finish()
            }
            .setNegativeButton("НЕТ", null)
            .setCancelable(true)
            .create()
            .show()
    }*/

    fun checkAnimalDelete(animal: Animal? = AnimalRepository.getInstance().animal.value) {
        if (animal == null) return
        val s = animal.name
        AlertDialog.Builder(this)
            .setTitle("УДАЛЕНИЕ!")
            .setMessage("Вы действительно хотите удалить животное $s ?")
            .setPositiveButton("ДА") { _, _ ->
                AnimalRepository.getInstance().deleteAnimal(animal)
            }
            .setNegativeButton("НЕТ", null)
            .setCancelable(true)
            .create()
            .show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        /*val toast = Toast.makeText(applicationContext, AppAviaryIntendApplication.applicationContext().toString(), Toast.LENGTH_LONG)
        toast.show()*/
        inflater.inflate(R.menu.animal_menu, menu)

        miAnAdd=menu.findItem(R.id.miAnAdd)
        miAnDelete=menu.findItem(R.id.miAnDelete)
        miAnChange=menu.findItem(R.id.miAnChange)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.miAnAdd -> {
                showNewAnimal()
                true
            }
            R.id.miAnDelete -> {
                checkAnimalDelete()
                true
            }
            R.id.miAnChange -> {
                showAnimalInfo()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun showNewAnimal() {
        AnimalRepository.getInstance().newAnimal()
        showAnimalInfo()
    }

    fun showAnimalsList() {
        miAnAdd?.isVisible=true
        miAnDelete?.isVisible=true
        miAnChange?.isVisible=true
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.anFrame, AnimalsListFragment.getInstance(),
                MyConstants.ANIMAL_LIST_FRAGMENT_TAG
            )
            .commit()
    }

    fun showAnimalInfo() {
        miAnAdd?.isVisible=false
        miAnDelete?.isVisible=false
        miAnChange?.isVisible=false
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.anFrame, AnimalInfoFragment.getInstance(),
                MyConstants.ANIMAL_INFO_FRAGMENT_TAG
            )
            //.addToBackStack(null)
            .commit()
    }

}