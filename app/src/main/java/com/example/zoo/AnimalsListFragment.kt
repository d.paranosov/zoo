package com.example.zoo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zoo.MyConstants.TAG
import com.example.zoo.data.Animal
import com.example.zoo.data.AnimalsList

class AnimalsListFragment : Fragment() {

    private lateinit var animalsListViewModel: AnimalsListViewModel
    private lateinit var animalsListRecyclerView: RecyclerView

    companion object {
        private var INSTANCE : AnimalsListFragment? = null

        fun getInstance(): AnimalsListFragment {
            if (INSTANCE == null) {
                INSTANCE = AnimalsListFragment()
            }
            return INSTANCE?: throw IllegalStateException("Отображение списка не создано!")
        }
    }

    private lateinit var viewModel: AnimalsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layoutView = inflater.inflate(R.layout.list_of_animals, container, false)
        animalsListRecyclerView = layoutView.findViewById(R.id.rvList)
        animalsListRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        return layoutView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        animalsListViewModel = ViewModelProvider(this).get(AnimalsListViewModel::class.java)
        animalsListViewModel.animalsList.observe(viewLifecycleOwner){
            updateUI(it)
        }
    }

    private inner class AnimalListAdapter(private val orderItems: List<Animal>) : RecyclerView.Adapter<AnimalHolder>()
    {
        override fun onCreateViewHolder(
            parrent : ViewGroup,
            viewType :Int
        ):AnimalHolder{
            val view = layoutInflater.inflate(R.layout.animals_list_element, parrent, false)
            return AnimalHolder(view)
        }

        override fun getItemCount(): Int = orderItems.size

        override fun onBindViewHolder(holder: AnimalHolder, position: Int) {
            holder.bind(orderItems[position])
        }

    }

    private inner class AnimalHolder(view : View)
        : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        private lateinit var animal: Animal
        private val nameTextView : TextView = itemView.findViewById(R.id.tvName)
        private val ageTextView : TextView = itemView.findViewById(R.id.tvAge)
        private val classTextView : TextView = itemView.findViewById(R.id.tvClass)
        //private val aviaryTextView : TextView = itemView.findViewById(R.id.tvAviary)
        private val clLayout: ConstraintLayout = itemView.findViewById(R.id.clCL)

        fun bind(animal: Animal){
            Log.d(MyConstants.TAG, "bind 1 $animal")
            this.animal = animal
            clLayout.setBackgroundColor(context!!.getColor(R.color.white))
            if (animal.id == animalsListViewModel.animal.id)
                clLayout.setBackgroundColor(context!!.getColor(R.color.element))
            nameTextView.text = animal.name
            //aviaryTextView.text = animal.aviary
            classTextView.text = animal.animalClass
            ageTextView.text = animal.age.toString()
            Log.d(MyConstants.TAG, "bind 2 $animal")
        }

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            Log.d(TAG, "AnimalHolder onClick")
            animalsListViewModel.setAnimal(animal)
            (requireActivity() as AnimalActivity).showAnimalInfo()
            //animalsListRecyclerView.adapter = AnimalListAdapter(animalsListViewModel.animalsList.value!!.items)
        }

        override fun onLongClick(v: View?): Boolean {
            animalsListViewModel.setAnimal(animal)
            (requireActivity() as AnimalActivity).checkAnimalDelete(animal)
            return true
        }

    }

    private fun updateUI(animalsList: AnimalsList?=null){
        if (animalsList==null) return
        animalsListRecyclerView.adapter = AnimalListAdapter(animalsList.items)
        animalsListRecyclerView.layoutManager?.scrollToPosition(animalsList.items.size)
    }

}